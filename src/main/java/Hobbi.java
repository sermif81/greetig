public class Hobbi implements Greeting {
    private String FirstName;
    private String SecondName;
    private String LastName;
    private String Hobbies;
    private String BitbucketUrl;
    private String Phone;



    private String CourseExpectation;
    private String EducationInfo;
    private String id, name, description;

    public Hobbi(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Hobbi(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getBitbucketUrl() {
        return BitbucketUrl;
    }

    public void setBitbucketUrl(String bitbucketUrl) {
        BitbucketUrl = bitbucketUrl;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getCourseExpectation() {
        return CourseExpectation;
    }

    public void setCourseExpectation(String courseExpectation) {
        CourseExpectation = courseExpectation;
    }

    public String getEducationInfo() {
        return EducationInfo;
    }

    @Override
    public String toString() {
        return "Hobbi{" +
                "\nFirstName='" + FirstName + '\'' +
                ", \n SecondName='" + SecondName + '\'' +
                ", \n LastName='" + LastName + '\'' +
                ", \nHobbies='" + Hobbies + '\'' +
                ", \nBitbucketUrl='" + BitbucketUrl + '\'' +
                ", \nPhone='" + Phone + '\'' +
                ", \nCourseExpectation='" + CourseExpectation + '\'' +
                ", \nEducationInfo='" + EducationInfo + '\'' +
                ", \nid='" + id + '\'' +
                ", \nname='" + name + '\'' +
                ", \ndescription='" + description + '\'' +
                '}';
    }

    public void setEducationInfo(String educationInfo) {
        EducationInfo = educationInfo;


    }
    @Override
    public void show() {
        System.out.println(this.FirstName + " "  + this.LastName);
        System.out.println(this.BitbucketUrl + " "  + this.getPhone());
        System.out.println(this.getCourseExpectation() + " "  + this.getHobbies());
        System.out.println(this.getEducationInfo() + " "  + this.getHobbies());
    }

    }

